import { CommonModule } from "@angular/common";
import { Component } from "@angular/core";

@Component({
    selector: 'app-sign-layout',
    templateUrl: 'sign.layout.html',
    styleUrl: 'sign.layout.scss',
    standalone: true,
    imports: [CommonModule]
})
export class SignLayout {

}