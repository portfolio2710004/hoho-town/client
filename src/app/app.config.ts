import { provideHttpClient, withInterceptors } from '@angular/common/http';
import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';

import { publicInterceptor } from 'src/interceptor/public.interceptor';
import { credentialInterceptor } from 'src/interceptor/credential.interceptor';

import { routes } from './app.routes';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    provideHttpClient(
      withInterceptors([
        publicInterceptor,
        credentialInterceptor
      ])
    )
  ]
};
