import { Component } from "@angular/core";

@Component({
    selector: 'app-example-page',
    templateUrl: './example.page.html',
    styleUrls: ['./example.page.scss'],
    standalone: true
})
export class ExamplePage {

}