import { Component, DestroyRef } from "@angular/core";
import { takeUntilDestroyed } from "@angular/core/rxjs-interop";
import { ActivatedRoute, Router } from "@angular/router";

import { EMPTY, switchMap } from "rxjs";

import { ISnsSigninRequest } from "src/interface/auth/sns-signin.dto";
import { AuthService } from "src/service/auth.service";
import { KakaoService } from "src/service/kakao.service";

@Component({
    selector: 'app-kakao-redirect',
    template: '<div class="w-full h-screen bg-gray-800"></div>',
    standalone: true
})
export class KakaoRedirectPage {

    constructor(
        private readonly route: ActivatedRoute,
        private readonly router: Router,
        private readonly destroyRef: DestroyRef,
        private readonly kakaoService: KakaoService,
        private readonly authService: AuthService
    ) {
        this.route.queryParams.pipe(
            switchMap(params => {
                const code = params['code'];
                if (!code) {
                    window.alert('카카오 로그인에 문제가 발생했습니다.');
                    this.router.navigateByUrl('/');
                    return EMPTY;
                }
                return this.kakaoService.getTokenByCode(code);
            }),
            switchMap(tokens => {
                const { access_token } = tokens;
                const request: ISnsSigninRequest = { provider: 'kakao', token: access_token };
                return this.authService.snsSignin(request);
            }),
            takeUntilDestroyed(this.destroyRef)
        ).subscribe();
    }
}