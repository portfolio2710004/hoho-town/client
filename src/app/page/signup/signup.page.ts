import { CommonModule } from "@angular/common";
import { Component } from "@angular/core";
import { FormBuilder, FormGroup, ReactiveFormsModule } from "@angular/forms";
import { Router } from "@angular/router";

import { SignLayout } from "src/app/layout/sign/sign.layout";
import { ISnsSigninRequest } from "src/interface/auth/sns-signin.dto";
import { SpriteSheets } from "src/interface/phaser/sprite.type";
import { ICreateSnsUserRequest } from "src/interface/user/create-sns-user.dto";
import { AuthService } from "src/service/auth.service";

import { BirdProps, birds } from "./signup.resource";

@Component({
    selector: 'app-signup',
    templateUrl: 'signup.page.html',
    standalone: true,
    imports: [
        ReactiveFormsModule,
        CommonModule,
        SignLayout,
    ]
})
export class SignupPage {

    readonly formGroup!: FormGroup;

    readonly birds: BirdProps[] = birds;

    selectBird?: BirdProps = this.birds.find(bird => bird.id === 0);

    constructor(
        private readonly router: Router,
        private readonly fb: FormBuilder,
        private readonly authService: AuthService 
    ) {
        const currentNavigation = this.router.getCurrentNavigation();
        const state = currentNavigation?.extras.state as ISnsSigninRequest;
    
        if (!state) {
            window.alert('정상적으로 회원가입을 진행할 수 없습니다.');
            this.router.navigateByUrl('/');
            return;
        }

        this.formGroup = this.fb.group({
            'provider': [state.provider],
            'token': [state.token],
            'nickname': [''],
            'gender': [''],
            'birthday': ['']
        });
    }

    onSelectBirdProps(bird: BirdProps) {
        this.selectBird = bird;
    }

    onSignup() {
        const form = this.formGroup.value;

        if (!form.nickname) {
            window.alert('닉네임을 입력해 주세요.');
            return;
        }

        if (!form.birthday) {
            window.alert('생일을 입력해 주세요.');
            return;
        }

        if (!form.birthday) {
            window.alert('성별을 입력해 주세요.');
            return;
        }
 
        const request: ICreateSnsUserRequest = {
            token: form.token,
            provider: form.provider,
            birthday: form.birthday,
            gender: form.gender,
            nickname: form.nickname,
            spriteSheet: this.selectBird?.spriteSheet ?? SpriteSheets.BIRD01
        }

        this.authService.signup(request).subscribe();
    }
}