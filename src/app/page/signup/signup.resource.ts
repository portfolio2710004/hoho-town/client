import { SpriteSheet, SpriteSheets } from "src/interface/phaser/sprite.type";

export type BirdProps = {
    id: number;
    url: string;
    name: string;
    comment: string;
    spriteSheet: SpriteSheet;
}

export const birds: BirdProps[] = [
    { id: 0, spriteSheet: SpriteSheets.BIRD01, url: 'assets/bird01.gif', name: '파랑새', comment: '파랗다. 나름 첫번째 새의 자부심을 가지고 있다.' },
    { id: 1, spriteSheet: SpriteSheets.BIRD02, url:'assets/bird02.gif', name: '빨강새', comment: '빨갛다. 통통한 매력이 있다.' },
    { id: 2, spriteSheet: SpriteSheets.BIRD03, url: 'assets/bird03.gif', name: '독수리', comment: '만들고 보니 갈매기 다음으로 인기 없을 것 같다..' },
    { id: 3, spriteSheet: SpriteSheets.BIRD04, url: 'assets/bird04.gif', name: '괘씸한 갈매기', comment: '괘씸한 빨간 두건을 두르고 있다. 제일 인기 없을 것 같다.' },
    { id: 4, spriteSheet: SpriteSheets.BIRD05, url: 'assets/bird05.gif', name: '홍학', comment: '구글에 실제로 새 이름을 검색해보면 안귀엽지만, 이녀석은 귀엽다.' },
    { id: 5, spriteSheet: SpriteSheets.BIRD06, url: 'assets/bird06.gif', name: '딱따구리', comment: '제일 그리기 쉬웠던 녀석이다.' },
    { id: 6, spriteSheet: SpriteSheets.BIRD07, url: 'assets/bird07.gif', name: '참새', comment: '짹짹-' },
    { id: 7, spriteSheet: SpriteSheets.BIRD08, url: 'assets/bird08.gif', name: '오리', comment: '이녀석으로 테스트를 하면 항상 벽을 뚥곤 했다. 좋은 테스터다. (삭제할까 고민도 했다.)' },
    { id: 8, spriteSheet: SpriteSheets.BIRD09, url: 'assets/bird09.gif', name: '키위새', comment: '먹지마세요.' },
];