import { Component, DestroyRef, WritableSignal, afterNextRender, computed, signal } from "@angular/core";
import { toSignal } from "@angular/core/rxjs-interop";

import { filter, switchMap } from "rxjs";

import { IGetMyProfileResponse } from "src/interface/user/get-my-profile.dto";
import { IGetUsersResponse } from "src/interface/user/get-users.dto";
import { AuthService } from "src/service/auth.service";
import { PhaserService } from "src/service/phaser.service";
import { ProfileService } from "src/service/profile.service";
import { UserListService } from "src/service/user-list.service";

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    standalone: true
})
export class HomePage {

    readonly users: WritableSignal<IGetUsersResponse[]> = signal([]);

    readonly profile = toSignal(this.profileService.profile$);
    
    private phaserService!: PhaserService;

    constructor(
        private readonly destroyRef: DestroyRef,
        private readonly authService: AuthService,
        private readonly profileService: ProfileService,
        private readonly userListService: UserListService,
    ) {
        this.profileService.initProfile().subscribe();
        this.userListService.initUsers().subscribe();

        // 캔버스가 로드되면 페이저 실행
        afterNextRender(() => {
            this.phaserService = PhaserService.getInstance();

            this.phaserService.levelSceneLoaded$.pipe(
                filter(value => value), 
                switchMap(() => this.profileService.profile$)
            ).subscribe(profile => this.onPhaserEvents(profile));
        });

        // 페이지가 제거될 때 페이저 모듈 제거
        this.destroyRef.onDestroy(() => this.phaserService.onDestroy());
    }

    private onPhaserEvents(profile: IGetMyProfileResponse|null) {
        if (!profile) return;
        
        this.phaserService.emitJoin({
            ...profile,
            anim: 'idle',
            x: 0,
            y: 0,
            velocityX: 0,
            velocityY: 0
        });

        this.phaserService.onDuplicatePlayer(message => {
            window.alert(message);
            this.authService.clearSession();
        });

        this.phaserService.onLeaveOtherPlayer(() => {
            console.log('다른 플레이어 떠남!');
        });
    }
}