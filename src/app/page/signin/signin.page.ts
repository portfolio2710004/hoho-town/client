import { Component } from "@angular/core";

import { SignLayout } from "src/app/layout/sign/sign.layout";
import { KakaoService } from "src/service/kakao.service";

@Component({
    selector: 'app-signin',
    templateUrl: 'signin.page.html',
    standalone: true,
    imports: [SignLayout]
})
export class SigninPage {

    readonly kakaoAuthUrl: string;

    constructor(
        private readonly kakaoService: KakaoService
    ) {
        this.kakaoAuthUrl = this.kakaoService.getKakaoAuthUrl();
    }
}