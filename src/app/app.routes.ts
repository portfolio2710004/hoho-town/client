import { Routes } from '@angular/router';

import { CredentialGuard } from 'src/service/guard/credential.guard';

export const routes: Routes = [
    {
        path: '',
        loadComponent: () => import('./page/signin/signin.page').then(c => c.SigninPage)
    },
    {
        path: 'example',
        loadComponent: () => import('./page/example/example.page').then(c => c.ExamplePage)
    },
    {
        path: 'signup',
        loadComponent: () => import('./page/signup/signup.page').then(c => c.SignupPage)
    },
    {
        path: 'redirect/kakao',
        loadComponent: () => import('./page/redirect/kakao/kakao-redirect.page').then(c => c.KakaoRedirectPage)
    },
    {
        canActivate: [CredentialGuard],
        path: 'home',
        loadComponent: () => import('./page/home/home.page').then(c => c.HomePage)
    }
];
