export enum TokenTypes {
    ACCESS = 'ACCESS',
    REFRESH = 'REFRESH'
}

export type ITokenType = `${TokenTypes}`;

export type ITokenPayload = {
    aud: number;
    type: ITokenType;
};

export type ITokens = {
    accessToken: string;
    refreshToken: string;
}