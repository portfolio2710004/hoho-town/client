export enum WebSocketEvents {
    // 내가 방에 참가할 경우: client -> server
    JOIN = 'join', 

    // 내가 방에 참가가 완료 된 경우: server -> client 
    JOINED = 'joined',

    // 다른 사람이 참가 한 경우: server -> client
    JOIN_OTHER_PLAYERS = 'joinOthers',

    // 내 플레이어를 움직일 경우: client -> server
    UPDATE_PAYER = 'updatePlayer',

    // 다른 사람의 플레이어가 업데이트 되었을 경우: server -> client
    UPDATE_OTHER_PLAYERS = 'updateOtherPlayers',

    // 방에 참가했는데 같은 플레이어 아이디가 존재할 경우: server -> client
    DUPLICATE_PLAYER = 'duplicatePlayer',

    // 다른 플레이어의 세션이 종료된 경우: server -> client
    LEAVE_OTHER_PLAYER = 'disconnectOtherPlayer',
}

export type WebSocketEvent = `${WebSocketEvents}`;