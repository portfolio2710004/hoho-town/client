export enum SpriteSheets {
    BIRD01 = 'bird01-Sheet',
    BIRD02 = 'bird02-Sheet',
    BIRD03 = 'bird03-Sheet',
    BIRD04 = 'bird04-Sheet',
    BIRD05 = 'bird05-Sheet',
    BIRD06 = 'bird06-Sheet',
    BIRD07 = 'bird07-Sheet',
    BIRD08 = 'bird08-Sheet',
    BIRD09 = 'bird09-Sheet',
}

export type SpriteSheet = `${SpriteSheets}`;

export function getSpriteSheetArray(): SpriteSheet[] {
    return Object.values(SpriteSheets);
}