import { SpriteSheet } from "./sprite.type";

export enum Anims {
    IDLE = 'idle',
    UP = 'up',
    DOWN = 'down',
    LEFT = 'left',
    RIGHT = 'right',
    LEFT_UP = 'leftUp',
    LEFT_DOWN = 'leftDown',
    RIGHT_UP = 'rightUp',
    RIGHT_DOWN = 'rightDown'
}

export type Anim = `${Anims}`;

export type Player = {
    id: number;
    nickname: string;
    spriteSheet: SpriteSheet;
    anim: Anim;
    x: number,
    y: number,
    velocityX: number,
    velocityY: number,
}

export type PlayerWithOtherPlayer = [Player, Player[]];