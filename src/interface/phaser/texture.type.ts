export type TextureMapProps = {
    idle: string;
    move: string;
}