
import { Gender } from "../user/user.type";

import { IGetMyProfileResponse } from "./get-my-profile.dto";

export type IUpdateUserRequest = {
    nickname: string;
    birthday: string;
    gender: Gender;
}

export type IUpdateUserResponse = IGetMyProfileResponse;