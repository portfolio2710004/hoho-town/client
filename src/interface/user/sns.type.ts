export enum Sns {
  KAKAO = 'kakao',
  NAVER = 'naver',
  GOOGLE = 'google',
  FACEBOOK = 'facebook',
  APPLE = 'apple'
}

export type SnsType = `${Sns}`;

export type ISnsUser = {
  provider: SnsType;
  providerId: string;
  nickname: string;
  thumbnail?: string;
}

export function getSnsArray(): string[] {
  return Object.values(Sns);
}