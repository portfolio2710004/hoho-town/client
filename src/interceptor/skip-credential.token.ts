import { HttpContextToken } from "@angular/common/http";

export const SKIP_CREDENTIAL = new HttpContextToken<boolean>(() => false);
