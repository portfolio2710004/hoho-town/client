import { HttpEvent, HttpHandlerFn, HttpRequest } from "@angular/common/http";
import { inject } from "@angular/core";
import { Router } from "@angular/router";

import { Observable, catchError, throwError } from "rxjs";

import { ResponseErrorType } from "src/interface/error.type";
import { SKIP_CREDENTIAL } from "./skip-credential.token";

export function publicInterceptor(req: HttpRequest<unknown>, next: HttpHandlerFn): Observable<HttpEvent<unknown>> {

    const router = inject(Router);

    if (!req.context.get(SKIP_CREDENTIAL)) {
        return next(req);
    }

    return next(req).pipe(
        catchError((res: ResponseErrorType) => {
            if (res.status === 0) {
                window.alert('서버와 연결할 수 없습니다.');
                router.navigateByUrl('/');
            }
            return throwError(() => res);
        })
    );
}
