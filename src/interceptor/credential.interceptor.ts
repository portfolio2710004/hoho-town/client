import { HttpEvent, HttpHandlerFn, HttpRequest } from "@angular/common/http";
import { inject } from "@angular/core";
import { Router } from "@angular/router";

import { Observable, catchError, switchMap, throwError } from "rxjs";

import { ResponseErrorType } from "src/interface/error.type";
import { AuthService } from "src/service/auth.service";

import { SKIP_CREDENTIAL } from "./skip-credential.token";

export function credentialInterceptor(req: HttpRequest<unknown>, next: HttpHandlerFn): Observable<HttpEvent<unknown>> {

    const router = inject(Router);
    const authService = inject(AuthService);
    
    if (req.context.get(SKIP_CREDENTIAL)) {
        return next(req);
    }

    req = req.clone({
        setHeaders: { 'Authorization': `Bearer ${authService.getAccessToken()}` }
    });

    return next(req).pipe(
        catchError((res: ResponseErrorType) => {
            if (res.status === 0) {
                window.alert('서버와 연결할 수 없습니다.');
                router.navigateByUrl('/');
            }

            if (res.status === 401) {
                return authService.refresh().pipe(
                    switchMap((tokens) => 
                        next(req.clone({
                            setHeaders: { 'Authorization': `Bearer ${tokens.accessToken}`}
                        }))
                    )
                );
            }

            return throwError(() => res);
        })
    );
}
