
// You can write more code here

/* START OF COMPILED CODE */

import Phaser from "phaser";
/* START-USER-IMPORTS */
import { Anim, Anims, Player, PlayerWithOtherPlayer } from "src/interface/phaser/player.type";
import { SpriteSheet } from "src/interface/phaser/sprite.type";
import { PhaserService } from "src/service/phaser.service";
import PlayerPrefab from "../prefabs/PlayerPrefab";
/* END-USER-IMPORTS */

export default class Level extends Phaser.Scene {

	constructor() {
		super("Level");

		/* START-USER-CTR-CODE */

		/* END-USER-CTR-CODE */
	}

	editorCreate(): void {

		// leftKey
		const leftKey = this.input.keyboard!.addKey(Phaser.Input.Keyboard.KeyCodes.LEFT);

		// rightKey
		const rightKey = this.input.keyboard!.addKey(Phaser.Input.Keyboard.KeyCodes.RIGHT);

		// upKey
		const upKey = this.input.keyboard!.addKey(Phaser.Input.Keyboard.KeyCodes.UP);

		// downKey
		const downKey = this.input.keyboard!.addKey(Phaser.Input.Keyboard.KeyCodes.DOWN);

		// tilemap
		const tilemap = this.add.tilemap("tilemap2");
		tilemap.addTilesetImage("tilemap-assets", "tilemap-assets");

		// layer1
		const layer1 = tilemap.createLayer("bottom", ["tilemap-assets"], 0, 0)!;

		// layer2
		const layer2 = tilemap.createLayer("behindWallLayer1", ["tilemap-assets"], 0, 0)!;

		// layer3
		const layer3 = tilemap.createLayer("behindWallLayer2", ["tilemap-assets"], 0, 0)!;

		// layer4
		const layer4 = tilemap.createLayer("behindObjectLayer1", ["tilemap-assets"], 0, 0)!;

		// layer5
		const layer5 = tilemap.createLayer("behindObjectLayer2", ["tilemap-assets"], 0, 0)!;

		// layer6
		const layer6 = tilemap.createLayer("behindObjectLayer3", ["tilemap-assets"], 0, 0)!;

		// layer7
		const layer7 = tilemap.createLayer("behindItemLayer1", ["tilemap-assets"], 0, 0)!;

		// layer8
		const layer8 = tilemap.createLayer("behindItemLayer2", ["tilemap-assets"], 0, 0)!;

		// layer9
		const layer9 = tilemap.createLayer("behindItemLayer3", ["tilemap-assets"], 0, 0)!;

		// layer10
		const layer10 = tilemap.createLayer("frontObjectLayer1", ["tilemap-assets"], 0, 0)!;

		// layer11
		const layer11 = tilemap.createLayer("frontObjectLayer2", ["tilemap-assets"], 0, 0)!;

		// layer12
		const layer12 = tilemap.createLayer("frontObjectLayer3", ["tilemap-assets"], 0, 0)!;

		// layer13
		const layer13 = tilemap.createLayer("frontWallLayer1", ["tilemap-assets"], 0, 0)!;

		// layer14
		const layer14 = tilemap.createLayer("frontWallLayer2", ["tilemap-assets"], 0, 0)!;

		// lists
		const tileLayers = [layer1, layer2, layer3, layer4, layer5, layer6, layer7, layer8, layer9, layer10, layer11, layer12, layer13, layer14];

		this.leftKey = leftKey;
		this.rightKey = rightKey;
		this.upKey = upKey;
		this.downKey = downKey;
		this.tilemap = tilemap;
		this.tileLayers = tileLayers;

		this.events.emit("scene-awake");
	}

	private leftKey!: Phaser.Input.Keyboard.Key;
	private rightKey!: Phaser.Input.Keyboard.Key;
	private upKey!: Phaser.Input.Keyboard.Key;
	private downKey!: Phaser.Input.Keyboard.Key;
	private tilemap!: Phaser.Tilemaps.Tilemap;
	private tileLayers!: Phaser.Tilemaps.TilemapLayer[];

	/* START-USER-CODE */
	private player!: PlayerPrefab;
	private otherPlayers: PlayerPrefab[] = [];
	private otherPlayerNames: Phaser.GameObjects.Text[] = [];

	private phaserService!: PhaserService;

	create() {
		this.editorCreate();

		// 캐릭터와 월드를 정확하게 충돌 시키기 위해 타일맵의 실제 크기 구하기
		const worldWidth = 992;
		const worldHeight = 320;

		this.physics.world.bounds.width = worldWidth;
		this.physics.world.bounds.height = worldHeight;

		// 타일맵 레이어를 순회하며 깊이 설정해줘야함 -> 캐릭터와 오브젝트간의 우선순위 설정
		this.tileLayers.map((tile, index) => tile.setDepth(index));

		// 타일맵에서 만든 충돌 오브젝트 목록을 가져오고 물리설정을 적용
		const collidingObjects = this.tilemap.createFromObjects('collideObjects', { classType: Phaser.Physics.Arcade.Image });
		this.physics.world.enable(collidingObjects);
		collidingObjects.forEach((obj) => {
			const arcadeObj = obj as Phaser.Physics.Arcade.Image;
			arcadeObj.setSize(arcadeObj.width, arcadeObj.height); // 오브젝트의 넓이 높이값 적용
			arcadeObj.setImmovable(true); // 오브젝트를 정적으로 설정 -> 안해주면 충돌시 가속도, 무게에 비례해서 튕겨나감
			arcadeObj.setVisible(false); // 충돌 오브젝트들은 타일맵이 아니라 도형을 이용해서 만듬 -> 보여줄 필요 없음
		});

		// 사용자가 접속 할 경우 앵귤러에서 이벤트를 발행하고 - 페이저에 이벤트를 감지하여 플레이어 정보를 통해 플레이어 생성
		this.phaserService = PhaserService.getInstance();

		// 레벨씬이 로드되면 상태를 업데이트함 -> 앵귤러에서 상태를 감지하고 소켓 이벤트를 받을 수 있게 준비
		this.phaserService.updateLevelSceneLoaded();

		// 플레이어가 입장 했을 경우 이벤트
		this.phaserService.onJoined(([player, otherPlayers]: PlayerWithOtherPlayer) => {
			const spawn = this.tilemap.findObject("objects", obj => obj.name === "spawn") as { x: number, y: number };

			this.player = this.createPlayerPrefab(spawn.x, spawn.y, player);
			this.cameras.main.startFollow(this.player, true, 0.1, 0.1);
			this.cameras.main.setZoom(2);

			for (let otherPlayer of otherPlayers) {
				const otherPlayerPrefab = this.createPlayerPrefab(
					otherPlayer.x !== 0 ? otherPlayer.x : spawn.x, 
					otherPlayer.y !== 0 ? otherPlayer.y : spawn.y,
					otherPlayer
				);
				const otherPlayerName = this.add.text(
					otherPlayerPrefab.body.x, 
					otherPlayerPrefab.body.y, 
					otherPlayer.nickname, {
						font: '300 16px'
					}
				);
				otherPlayerPrefab.setNicknameText(otherPlayerName);
				this.otherPlayers.push(otherPlayerPrefab);
			}

			// 플레이어와 충돌레이어간의 충돌 설정
			this.physics.add.collider(this.player, collidingObjects);
			this.physics.add.collider(this.otherPlayers, collidingObjects);
		});

		// 다른 플레이어가 입장했을 경우 이벤트
		this.phaserService.onOtherJoin((otherPlayer: Player) => {
			const spawn = this.tilemap.findObject("objects", obj => obj.name === "spawn") as { x: number, y: number };
			const otherPlayerPrefab = this.createPlayerPrefab(spawn.x, spawn.y, otherPlayer);
			const otherPlayerName = this.add.text(
				otherPlayerPrefab.body.x, 
				otherPlayerPrefab.body.y, 
				otherPlayer.nickname, {
					font: '300 16px'
				}
			);
			otherPlayerPrefab.setNicknameText(otherPlayerName);
			this.otherPlayers.push(otherPlayerPrefab);

			this.physics.add.collider(otherPlayerPrefab, collidingObjects);
		});

		// 다른 플레이어가 행동을 할 경우 이벤트
		this.phaserService.onUpdateOtherPlayer((otherPlayer: Player) => {
			this.otherPlayers.forEach(otherPlayerPrefab => {
				if (otherPlayer.id !== otherPlayerPrefab.id) return;

				const lerpFactor = 0.3; // 0과 1 사이의 값
				
				otherPlayerPrefab.setAnim(otherPlayer.anim);

				otherPlayerPrefab.x += (otherPlayer.x - otherPlayer.x) * lerpFactor;
				otherPlayerPrefab.y += (otherPlayer.y - otherPlayer.y) * lerpFactor;

				otherPlayerPrefab.setVelocityX(otherPlayer.velocityX);
				otherPlayerPrefab.setVelocityY(otherPlayer.velocityY);

				otherPlayerPrefab.updateNicknameText();

				if (otherPlayer.anim === Anims.IDLE) {
					otherPlayerPrefab.setVelocity(0);
				}
			});
		});

		// 다른 플레이어가 떠났을 경우 이벤트
		this.phaserService.onLeaveOtherPlayer((id: number) => {
			const otherPlayerPrefab = this.otherPlayers.find(otherPlayer => otherPlayer.id === id);
			if (!otherPlayerPrefab) return;

			// 프리팹 인스턴스 제거
			otherPlayerPrefab.destroy();

			// otherPlayers 배열에서도 제거
			this.otherPlayers = this.otherPlayers.filter(otherPlayer => otherPlayer.id !== id);
		});
	}

	override update() {
		if (!this.player) {
			return;
		}

		this.player.body.setVelocity(0);

		if (this.leftKey.isUp && this.rightKey.isUp && this.upKey.isUp && this.downKey.isUp) {
			if (this.player.anim !== Anims.IDLE) {
				this.updatePlayerEvent(Anims.IDLE);
			}
			this.player.playIdle();
		}

		if (this.leftKey.isDown && this.upKey.isDown) {
			this.player.moveLeftUp();
			this.updatePlayerEvent(Anims.LEFT_UP);
		} else if (this.rightKey.isDown && this.upKey.isDown) {
			this.player.moveRightUp();
			this.updatePlayerEvent(Anims.RIGHT_UP);
		} else if (this.leftKey.isDown && this.downKey.isDown) {
			this.player.moveLeftDown();
			this.updatePlayerEvent(Anims.LEFT_DOWN);
		} else if (this.rightKey.isDown && this.downKey.isDown) {
			this.player.moveRightDown();
			this.updatePlayerEvent(Anims.RIGHT_DOWN);
		}

		if (this.leftKey.isDown) {
			this.player.moveLeft();
			this.updatePlayerEvent(Anims.LEFT);
		} else if (this.rightKey.isDown) {
			this.player.moveRight();
			this.updatePlayerEvent(Anims.RIGHT);
		} else if (this.upKey.isDown) {
			this.player.moveUp();
			this.updatePlayerEvent(Anims.UP);
		} else if (this.downKey.isDown) {
			this.player.moveDown();
			this.updatePlayerEvent(Anims.DOWN);
		}
	}

	private createPlayerPrefab(x: number, y: number, playerProps: Player) {
		const player = new PlayerPrefab(this, x, y, playerProps.spriteSheet);
		player.id = playerProps.id;
		player.nickname = playerProps.nickname;
		this.add.existing(player);
		player.setDepth(8);
		return player;
	}

	private updatePlayerEvent(anim: Anim) {
		this.phaserService.emitUpdatePlayer({
			id: this.player.id as number,
			nickname: this.player.nickname,
			x: this.player.x,
			y: this.player.y,
			velocityX: this.player.body.velocity.x,
			velocityY: this.player.body.velocity.y,
			spriteSheet: this.player.texture.key as SpriteSheet,
			anim,
		});
	}

	/* END-USER-CODE */
}

/* END OF COMPILED CODE */
