
// You can write more code here

/* START OF COMPILED CODE */

import Phaser from "phaser";
import PreloadBarUpdaterScript from "../script-nodes/PreloadBarUpdaterScript";
/* START-USER-IMPORTS */
import assetPackUrl from "src/assets/phaser3/asset-pack.json";
/* END-USER-IMPORTS */

export default class Preload extends Phaser.Scene {

	constructor() {
		super("Preload");

		/* START-USER-CTR-CODE */
		// Write your code here.
		/* END-USER-CTR-CODE */
	}

	editorCreate(): void {

		// progressBar
		const progressBar = this.add.rectangle(128, 10, 256, 20);
		progressBar.scaleX = 0.5;
		progressBar.scaleY = 0.5;
		progressBar.isFilled = true;
		progressBar.fillColor = 14737632;

		// preloadUpdater
		new PreloadBarUpdaterScript(progressBar);

		// progressBarBg
		const progressBarBg = this.add.rectangle(128, 10, 256, 20);
		progressBarBg.scaleX = 0.5;
		progressBarBg.scaleY = 0.5;
		progressBarBg.fillColor = 14737632;
		progressBarBg.isStroked = true;

		// loadingText
		const loadingText = this.add.text(114.5, 11, "", {});
		loadingText.setOrigin(0.5, 0.5);
		loadingText.text = "Welcome Hohocompany !";
		loadingText.setStyle({ "color": "#e0e0e0", "fontFamily": "arial", "fontSize": "20px" });

		// guapen
		const guapen = this.add.image(-0.0000010728836059570312, -3.5762786865234375e-7, "guapen_1");
		guapen.scaleX = 0.2;
		guapen.scaleY = 0.2;

		this.progressBar = progressBar;
		this.progressBarBg = progressBarBg;
		this.loadingText = loadingText;
		this.guapen = guapen;

		this.events.emit("scene-awake");
	}

	private progressBar!: Phaser.GameObjects.Rectangle;
	private progressBarBg!: Phaser.GameObjects.Rectangle;
	private loadingText!: Phaser.GameObjects.Text;
	private guapen!: Phaser.GameObjects.Image;

	/* START-USER-CODE */

	// Write your code here

	preload() {

		this.editorCreate();

		const music = this.sound.add('hohotown', {
            volume: 0.3,
            loop: true
        });
        music.play();

		this.guapen.setPosition(this.sys.canvas.width / 2.32, this.sys.canvas.height / 2.2);
		this.loadingText.setPosition(this.sys.canvas.width / 2, this.sys.canvas.height / 2);
		this.progressBar.setPosition(this.sys.canvas.width / 1.93, this.sys.canvas.height / 2.17);
		this.progressBarBg.setPosition(this.sys.canvas.width / 1.93, this.sys.canvas.height / 2.17);

		this.load.pack("asset-pack", assetPackUrl);
	}

	create() {
		this.scene.start("Level");
	}

	/* END-USER-CODE */
}

/* END OF COMPILED CODE */

// You can write more code here
