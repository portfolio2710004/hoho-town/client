
// You can write more code here

/* START OF COMPILED CODE */

import Phaser from "phaser";
/* START-USER-IMPORTS */
import { SpriteSheets } from "src/interface/phaser/sprite.type";
import { TextureMapProps } from "src/interface/phaser/texture.type";
import { ANIM_BIRD1IDLE, ANIM_BIRD1MOVE, ANIM_BIRD2IDLE, ANIM_BIRD2MOVE, ANIM_BIRD3IDLE, ANIM_BIRD3MOVE, ANIM_BIRD4IDLE, ANIM_BIRD4MOVE, ANIM_BIRD5IDLE, ANIM_BIRD5MOVE, ANIM_BIRD6IDLE, ANIM_BIRD6MOVE, ANIM_BIRD7IDLE, ANIM_BIRD7MOVE, ANIM_BIRD8IDLE, ANIM_BIRD8MOVE, ANIM_BIRD9IDLE, ANIM_BIRD9MOVE } from './playerAnimations';
import { Anims } from "src/interface/phaser/player.type";
/* END-USER-IMPORTS */

export default interface PlayerPrefab {

	body: Phaser.Physics.Arcade.Body;
}

export default class PlayerPrefab extends Phaser.Physics.Arcade.Sprite {

	constructor(scene: Phaser.Scene, x?: number, y?: number, texture?: string, frame?: number | string) {
		super(scene, x ?? 0, y ?? 0, texture || SpriteSheets.BIRD01, frame ?? 0);

		scene.physics.add.existing(this, false);
		this.body.setSize(32, 32, false);

		/* START-USER-CTR-CODE */
		this.awake();
		/* END-USER-CTR-CODE */
	}

	/* START-USER-CODE */

	public nickname: string = '';

	public id: number|undefined;

	public anim = Anims.IDLE;

	// 닉네임 텍스트 오브젝트를 저장
	private nicknameText?: Phaser.GameObjects.Text;

	// 스프라이트 키에 따라 일치하는 애니메이션키를 저장
	private readonly textureMap: Map<string, TextureMapProps> = new Map();

	// 플레이어 속도
	private readonly speed = 80;

	public playIdle() {
		this.body.setVelocity(0);
		this.setAnim(Anims.IDLE);
	}

	public moveLeftUp() {
		this.body.setVelocity(-this.speed / 2, -this.speed / 2);
		this.setAnim(Anims.LEFT_UP);
	}

	public moveRightUp() {
		this.body.setVelocity(this.speed / 2, -this.speed / 2);
		this.setAnim(Anims.RIGHT_UP);
	}

	public moveLeftDown() {
		this.body.setVelocity(-this.speed / 2, this.speed / 2);
		this.setAnim(Anims.LEFT_DOWN);
	}

	public moveRightDown() {
		this.body.setVelocity(this.speed / 2, this.speed / 2);
		this.setAnim(Anims.RIGHT_DOWN);
	}

	public moveUp() {
		this.body.setVelocityY(-this.speed);
		this.setAnim(Anims.UP);
	}

	public moveDown() {
		this.body.setVelocityY(this.speed);
		this.setAnim(Anims.DOWN);
	}

	public moveLeft() {
		this.body.setVelocityX(-this.speed);
		this.setAnim(Anims.LEFT);
	}

	public moveRight() {
		this.body.setVelocityX(this.speed);
		this.setAnim(Anims.RIGHT);
	}

	public setNicknameText(text: Phaser.GameObjects.Text) {
		this.nicknameText = text;
		this.nicknameText.setOrigin(0.5, 0.5);
		this.nicknameText.setDepth(15);
		this.updateNicknameText();
	}

	public updateNicknameText() {
		this.nicknameText?.setPosition(this.x, this.y - 12);
	}

	public setAnim(anim: string) {
		if (anim === 'leftUp') {
			this.play(this.textureMap.get(this.texture.key)!.move, true);
			this.setFlipX(true);
			this.anim = Anims.LEFT_UP;
		} else if (anim === 'rightUp') {
			this.play(this.textureMap.get(this.texture.key)!.move, true);
			this.setFlipX(false);
			this.anim = Anims.RIGHT_UP;
		} else if (anim === 'leftDown') {
			this.play(this.textureMap.get(this.texture.key)!.move, true);
			this.setFlipX(true);
			this.anim = Anims.LEFT_DOWN;
		} else if (anim === 'rightDown') {
			this.play(this.textureMap.get(this.texture.key)!.move, true);
			this.setFlipX(false);
			this.anim = Anims.RIGHT_DOWN;
		} 
		
		if (anim === 'left') {
			this.play(this.textureMap.get(this.texture.key)!.move, true);
			this.setFlipX(true);
			this.anim = Anims.LEFT;
		} else if (anim === 'right') {
			this.play(this.textureMap.get(this.texture.key)!.move, true);
			this.setFlipX(false);
			this.anim = Anims.RIGHT;
		} else if (anim === 'up') {
			this.play(this.textureMap.get(this.texture.key)!.move, true);
			this.anim = Anims.UP;
		} else if (anim === 'down') {
			this.play(this.textureMap.get(this.texture.key)!.move, true);
			this.anim = Anims.DOWN;
		} else if (anim === 'idle') {
			this.play(this.textureMap.get(this.texture.key)!.idle);
			this.anim = Anims.IDLE;
		}
	}

	/**
	 * 에셋등 초기 로드가 완료될 경우 
	 * 기본값 설정 
	 */
	private awake() {
		this.textureMap.set(SpriteSheets.BIRD01, { idle: ANIM_BIRD1IDLE, move: ANIM_BIRD1MOVE });
		this.textureMap.set(SpriteSheets.BIRD02, { idle: ANIM_BIRD2IDLE, move: ANIM_BIRD2MOVE });
		this.textureMap.set(SpriteSheets.BIRD03, { idle: ANIM_BIRD3IDLE, move: ANIM_BIRD3MOVE });
		this.textureMap.set(SpriteSheets.BIRD04, { idle: ANIM_BIRD4IDLE, move: ANIM_BIRD4MOVE });
		this.textureMap.set(SpriteSheets.BIRD05, { idle: ANIM_BIRD5IDLE, move: ANIM_BIRD5MOVE });
		this.textureMap.set(SpriteSheets.BIRD06, { idle: ANIM_BIRD6IDLE, move: ANIM_BIRD6MOVE });
		this.textureMap.set(SpriteSheets.BIRD07, { idle: ANIM_BIRD7IDLE, move: ANIM_BIRD7MOVE });
		this.textureMap.set(SpriteSheets.BIRD08, { idle: ANIM_BIRD8IDLE, move: ANIM_BIRD8MOVE });
		this.textureMap.set(SpriteSheets.BIRD09, { idle: ANIM_BIRD9IDLE, move: ANIM_BIRD9MOVE });

		this.setBodySize(8, 30);
		this.setOffset(13, 2);
		this.setScale(0.5);
		this.setCollideWorldBounds(true);
	}

	override destroy(fromScene?: boolean | undefined): void {
		super.destroy();
		this.nicknameText?.destroy();
	}

	/* END-USER-CODE */
}

/* END OF COMPILED CODE */

// You can write more code here
