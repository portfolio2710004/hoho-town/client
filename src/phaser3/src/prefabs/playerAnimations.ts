// The constants with the animation keys.

export const ANIM_BIRD1MOVE = "bird1Move";

export const ANIM_BIRD2MOVE = "bird2Move";

export const ANIM_BIRD3MOVE = "bird3Move";

export const ANIM_BIRD4MOVE = "bird4Move";

export const ANIM_BIRD5MOVE = "bird5Move";

export const ANIM_BIRD6MOVE = "bird6Move";

export const ANIM_BIRD7MOVE = "bird7Move";

export const ANIM_BIRD8MOVE = "bird8Move";

export const ANIM_BIRD9MOVE = "bird9Move";

export const ANIM_BIRD1IDLE = "bird1Idle";

export const ANIM_BIRD2IDLE = "bird2Idle";

export const ANIM_BIRD3IDLE = "bird3Idle";

export const ANIM_BIRD4IDLE = "bird4Idle";

export const ANIM_BIRD5IDLE = "bird5Idle";

export const ANIM_BIRD6IDLE = "bird6Idle";

export const ANIM_BIRD7IDLE = "bird7Idle";

export const ANIM_BIRD8IDLE = "bird8Idle";

export const ANIM_BIRD9IDLE = "bird9Idle";
