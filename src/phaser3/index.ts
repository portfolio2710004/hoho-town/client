import Phaser from "phaser";

import preloadAssetPackUrl from "src/assets/phaser3/preload-asset-pack.json";

export class Boot extends Phaser.Scene {
    constructor() {
        super("Boot");
    }

    preload() {
        this.load.pack("pack", preloadAssetPackUrl);
    }

    create() {
       this.scene.start("Preload");
    }
}