import { Injectable } from "@angular/core";

import { BehaviorSubject, tap } from "rxjs";

import { IGetMyProfileResponse } from "src/interface/user/get-my-profile.dto";

import { UserApi } from "./api/user.api";

@Injectable({
    providedIn: 'root'
})
export class ProfileService {
    
    private readonly state = new BehaviorSubject<IGetMyProfileResponse|null>(null);
    public readonly profile$ = this.state.asObservable();

    constructor(
        private readonly userApi: UserApi
    ) {}

    initProfile() {
        return this.userApi.getMyProfile().pipe(
            tap(profile => this.state.next(profile))
        );
    }

    clearProfile() {
        this.state.next(null);
    }
}