import { Socket, io } from "socket.io-client";

import { BehaviorSubject } from "rxjs";

import { environments } from "src/env/environments";
import { WebSocketEvents } from "src/interface/phaser/event.type";
import { Player } from "src/interface/phaser/player.type";

import { Boot } from "../phaser3";
import Level from "../phaser3/src/scenes/Level";
import Preload from "../phaser3/src/scenes/Preload";

export class PhaserService {

    static instance: PhaserService;

    private readonly socket: Socket;
    private readonly phaser: Phaser.Game;

    private readonly levelSceneLoadedState = new BehaviorSubject<boolean>(false);
    public readonly levelSceneLoaded$ = this.levelSceneLoadedState.asObservable(); 

    private constructor() {
        this.phaser = this.initPhaser();
        this.socket = io(environments.SERVER_URL);
    }

    static getInstance() {
        if (PhaserService.instance) {
            return PhaserService.instance;
        }
        const instance = new PhaserService();
        PhaserService.instance = instance;
        return instance;
    }

    updateLevelSceneLoaded() {
        this.levelSceneLoadedState.next(true);
    }

    // 세션에 연결 요청
    emitJoin(player: Player) {
        console.log('플레이어 입장!');
        this.socket.emit(WebSocketEvents.JOIN, player);
    }

    // 내 플레이어가 이동할 경우 데이터 전송
    emitUpdatePlayer(player: Player) {
		this.socket.emit(WebSocketEvents.UPDATE_PAYER, player);
	}

    // 동일한 계정이 다른 클라이언트에서 접속
    onDuplicatePlayer(callBack: (data: any) => void) {
        this.socket.on(WebSocketEvents.DUPLICATE_PLAYER, callBack);
    }

    // 세션에 연결되었을 경우
    onJoined(callBack: (data: any) => void) {
        this.socket.on(WebSocketEvents.JOINED, callBack);
    }

    // 다른 플레이어 접속
    onOtherJoin(callBack: (data: any) => void) {
        this.socket.on(WebSocketEvents.JOIN_OTHER_PLAYERS, callBack);
    }

    // 다른 플레이어가 이동
    onUpdateOtherPlayer(callBack: (data: any) => void) {
        this.socket.on(WebSocketEvents.UPDATE_OTHER_PLAYERS, callBack);
    }

    // 다른 플레이어가 세션 종료
    onLeaveOtherPlayer(callBack: (data: any) => void) {
        this.socket.on(WebSocketEvents.LEAVE_OTHER_PLAYER, callBack);
    }

    onDestroy() {
        this.socket.close();
        this.phaser?.destroy(true, false);
    }

    private initPhaser() {
        const canvasEl = document.querySelector('#canvas') as HTMLCanvasElement;
        if (!canvasEl) {
            const message = '페이저에 필요한 DOM 요소가 준비되지 않았습니다.'
            window.alert(message);
            throw new Error(message);
        }

        const game = new Phaser.Game({
            canvas: canvasEl,
            transparent: true,
            type: Phaser.WEBGL,
            render: {
                pixelArt: true // Nearest-Neighbor 필터링 활성화
            },
            physics: {
                default: 'arcade',
                arcade: {
                    // debug: true,
                }
            },
            scene: [Boot, Preload, Level]
        });
    
        game.scene.start("Boot");

        return game;
    }
}