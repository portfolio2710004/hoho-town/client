import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

import { EMPTY, catchError, tap, throwError } from "rxjs";

import { ITokens } from "src/interface/auth/auth.type";
import { ISnsSigninRequest } from "src/interface/auth/sns-signin.dto";
import { ResponseErrorType } from "src/interface/error.type";
import { ICreateSnsUserRequest } from "src/interface/user/create-sns-user.dto";

import { AuthApi } from "./api/auth.api";
import { UserApi } from "./api/user.api";

import { RefreshBehavior } from "./behavior/refresh.behavior";

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(
        private readonly router: Router,
        private readonly authApi: AuthApi,
        private readonly userApi: UserApi,
        private readonly refreshBehavior: RefreshBehavior
    ) {}

    snsSignin(request: ISnsSigninRequest) {
        return this.authApi.snsSignin(request).pipe(
            tap(tokens => {
                this.setSession(tokens);
                this.router.navigateByUrl('/home');
            }),
            catchError((err: ResponseErrorType) => {
                if (err.status === 404) {
                    window.alert('기존에 가입된 회원이 아닙니다. 회원가입을 진행해주세요.');
                    this.router.navigateByUrl('/signup', { state: {...request} });
                    return EMPTY;
                }
                return throwError(() => err.error.message);
            })
        );
    }

    signup(request: ICreateSnsUserRequest) {
        return this.userApi.createSnsUser(request).pipe(
            tap(tokens => {
                this.setSession(tokens);
                this.router.navigateByUrl('/home');
            })
        );
    }

    signout() {
        return this.authApi.signout().pipe(
            tap(this.clearSession)
        );
    }

    refresh() {
        return this.refreshBehavior.refresh(this);
    }

    getAccessToken() {
        const token = window.localStorage.getItem('accessToken');
        if (!token) return null;
        return token;
    }

    getRefreshToken() {
        const token = window.localStorage.getItem('refreshToken');
        if (!token) return null;
        return token;
    }

    setSession(tokens: ITokens) {
        localStorage.setItem('accessToken', tokens.accessToken);
        localStorage.setItem('refreshToken', tokens.refreshToken);
    }

    clearSession() {
        localStorage.removeItem('accessToken');
        localStorage.removeItem('refreshToken');
    }
}