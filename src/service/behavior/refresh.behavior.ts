import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

import { Subject, catchError, tap, throwError } from "rxjs";

import { ITokens } from "src/interface/auth/auth.type";
import { ResponseErrorType } from "src/interface/error.type";

import { AuthApi } from "../api/auth.api";
import { AuthService } from "../auth.service";

/**
 * 인증 서비스의 리프레시 기능은 복잡도가 높아서 
 * 행동 클래스로 추출하고 authService에 refresh 메서드가 호출시 사용되도록 함
 */
@Injectable({
    providedIn: 'root'
})
export class RefreshBehavior {

    private refreshTokenSubject: Subject<ITokens> = new Subject<ITokens>();
    private refreshTokenInProgress = false;

    constructor(
        private readonly router: Router,
        private readonly authApi: AuthApi
    ) { }

    refresh(authService: AuthService) {
        if (!this.refreshTokenInProgress) {
            console.log('토큰 재발급중.. --> 이후 요청 무시');
            this.refreshTokenInProgress = true;
            this.refreshTokenSubject = new Subject<any>();

            const refreshToken = authService.getRefreshToken();
            if (!refreshToken) {
                const response: ResponseErrorType = {
                    error: {
                        message: '세션이 만료되었습니다.',
                        statusCode: 401,
                    },
                    status: 401
                }
                return throwError(() => response);
            }

            this.authApi.refresh(refreshToken).pipe(
                tap((tokens: ITokens) => {
                    this.refreshTokenInProgress = false;
                    this.refreshTokenSubject.next(tokens);
                    authService.setSession(tokens);
                }),
                catchError((res: ResponseErrorType) => {
                    this.refreshTokenInProgress = false;
                    this.refreshTokenSubject.error(res);
                    window.alert('토큰이 만료되었습니다.');
                    this.router.navigateByUrl('/');
                    return throwError(() => res);
                })
            ).subscribe({ complete: () => console.log('토큰 재발급 완료') });
        }

        return this.refreshTokenSubject.asObservable();
    }
}