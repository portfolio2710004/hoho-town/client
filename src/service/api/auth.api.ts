import { HttpContext, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { SKIP_CREDENTIAL } from "src/interceptor/skip-credential.token";
import { IRefreshResponse } from "src/interface/auth/refresh.dto";
import { ISnsSigninRequest, ISnsSigninResponse } from "src/interface/auth/sns-signin.dto";

import { BaseApi } from "./_base.api";

@Injectable({
    providedIn: 'root'
})
export class AuthApi extends BaseApi {

    snsSignin(request: ISnsSigninRequest) {
        return this.httpClient.post<ISnsSigninResponse>(`${this.SERVER_URL}/v1/oauth/sns`, request, {
            context: new HttpContext().set(SKIP_CREDENTIAL, true)
        });
    }

    signout() {
        return this.httpClient.get(`${this.SERVER_URL}/v1/oauth/signout`);
    }

    refresh(refreshToken: string) {
        const headers = new HttpHeaders({
            'Authorization': `Bearer ${refreshToken}`
        });

        return this.httpClient.post<IRefreshResponse>(`${this.SERVER_URL}/v1/oauth/refresh`, {},
            { headers: headers, context: new HttpContext().set(SKIP_CREDENTIAL, true) }
        );
    }

}