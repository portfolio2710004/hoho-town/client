import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environments } from "src/env/environments";

@Injectable({
    providedIn: 'root'
})
export abstract class BaseApi {

    protected readonly SERVER_URL = `${environments.SERVER_URL}/api`;

    constructor(
        protected readonly httpClient: HttpClient
    ) { }
} 