import { HttpContext } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { SKIP_CREDENTIAL } from "src/interceptor/skip-credential.token";
import { ICreateSnsUserRequest, ICreateSnsUserResponse } from "src/interface/user/create-sns-user.dto";
import { IGetMyProfileResponse } from "src/interface/user/get-my-profile.dto";

import { BaseApi } from "./_base.api";


@Injectable({
    providedIn: 'root'
})
export class UserApi extends BaseApi {

    getMyProfile() {
        return this.httpClient.get<IGetMyProfileResponse>(`${this.SERVER_URL}/v1/users/profile`);
    }

    getUsers() {
        return this.httpClient.get<IGetMyProfileResponse[]>(`${this.SERVER_URL}/v1/users`);
    }

    createSnsUser(request: ICreateSnsUserRequest) {
        return this.httpClient.post<ICreateSnsUserResponse>(`${this.SERVER_URL}/v1/users`, request, {
            context: new HttpContext().set(SKIP_CREDENTIAL, true)
        });
    }

    // createTestUser(request: ICreateSnsUserRequest) {
    //     return this.httpClient.post<ICreateSnsUserResponse>(`${this.SERVER_URL}/v1/users/test`, request, {
    //         context: new HttpContext().set(SKIP_CREDENTIAL, true)
    //     });
    // }
}