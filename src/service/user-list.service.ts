import { Injectable } from "@angular/core";

import { BehaviorSubject, tap } from "rxjs";

import { IGetUsersResponse } from "src/interface/user/get-users.dto";
import { UserApi } from "./api/user.api";

export type UserStateProps = {
    id: number;
    nickname: string;
    thumbnail: string;
    online: boolean;
}

@Injectable({
    providedIn: 'root'
})
export class UserListService {

    private readonly state = new BehaviorSubject<IGetUsersResponse[]>([]);
    public readonly users$ = this.state.asObservable();

    constructor(
        private readonly userApi: UserApi
    ) {}

    initUsers() {
        return this.userApi.getUsers().pipe(
            tap(users => {
                const usersState = users.map(user => ({
                    ...user,
                    online: false
                })) as UserStateProps[];
                this.state.next(usersState);
            })
        );
    }

    addUser(user: UserStateProps) {
        const users = this.state.value;
        this.state.next([...users, user]);
    }

    updateUser(updatedUser: UserStateProps) {
        const updatedUsers = this.state.value.map(user => {
            if (user.id !== updatedUser.id) return user;
            return updatedUser;
        });
        this.state.next(updatedUsers);
    }

    removeUser(willRemovedUser: UserStateProps) {
        const filteringUsers = this.state.value.filter(user => user.id !== willRemovedUser.id);
        this.state.next(filteringUsers);
    }
}