import { inject } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivateFn, Router, RouterStateSnapshot } from "@angular/router";

import { AuthService } from "../auth.service";

export const CredentialGuard: CanActivateFn = (next: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
  const router= inject(Router);
  const credentialState = inject(AuthService);

  if (credentialState.getAccessToken()) return true;

  console.log('엑세스토큰이 준비되지 않음');
  router.navigateByUrl('/');
  return false;
};
