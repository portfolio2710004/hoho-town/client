import { HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { Observable } from "rxjs";

import { environments } from "src/env/environments";
import { KakaoTokens } from "src/interface/kakao/kakao.type";
import { BaseApi } from "./api/_base.api";

@Injectable({
    providedIn: 'root'
})
export class KakaoService extends BaseApi {
    private readonly KAKAO_CLIENT_ID = environments.KAKAO_CLIENT_ID;
    private readonly KAKAO_REDIRECT_URL = environments.KAKAO_REDIRECT_URL;

    getKakaoAuthUrl() {
        return 'https://kauth.kakao.com/oauth/authorize?response_type=code'
            + '&client_id=' + this.KAKAO_CLIENT_ID
            + '&redirect_uri=' + this.KAKAO_REDIRECT_URL;
    }

    getTokenByCode(code: string): Observable<KakaoTokens> {
        const headers = new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded'
        });

        const body = new URLSearchParams();
        body.set('grant_type', 'authorization_code');
        body.set('client_id', this.KAKAO_CLIENT_ID);
        body.set('redirect_uri', this.KAKAO_REDIRECT_URL);
        body.set('code', code);

        return this.httpClient.post<KakaoTokens>('https://kauth.kakao.com/oauth/token', body.toString(), { headers: headers });
    }
}