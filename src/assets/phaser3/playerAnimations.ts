// The constants with the animation keys.

export const ANIM_LEFT = "left";

export const ANIM_RIGHT = "right";

export const ANIM_UP = "up";

export const ANIM_DOWN = "down";

export const ANIM_IDLE = "idle";

export const ANIM_LEFT_IDLE = "left-idle";

export const ANIM_RIGHT_IDLE = "right-idle";

export const ANIM_UP_IDLE = "up-idle";
